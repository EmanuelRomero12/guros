import { connect } from 'mongoose';
import * as dotenv from 'dotenv';
dotenv.config();

export const mongoDB = async (): Promise<void> => {
	try {
		const mongoProd: string = process.env.MONGO_CNN ?? '';
		const mongoDev: string = 'mongodb://localhost:27017/guros';

		if (process.env.DEV === 'development') {
			await connect(mongoDev);
		} else {
			await connect(mongoProd);
		}
	} catch (error) {
		console.log(error);
		throw new Error('Database connection error');
	}
};
