import supertest from 'supertest';
import mongosee from 'mongoose';
import Server from '../../infrastructure/config/server.config';
import { mutationModel } from '../../infrastructure/models/mutation.model';
const app = new Server();

describe('testing the mutation path', () => {
	let token: string;
	beforeEach((done) => {
		supertest(app.app)
			.post('/api/auth/login')
			.send({ email: 'user.testing@test.com', password: 'test123' })
			.end((err, res) => {
				token = res.body.token;
				done();
			});
	});

	afterAll(async () => {
		await mongosee.disconnect();
		await mongosee.connection.close();
	});
	describe('POST - /api/mutation/', () => {
		describe('failed response test mutation path', () => {
			it('should respond with a status code 401 if the request does not have a jwt', async () => {
				await supertest(app.app).post('/api/mutation/').expect(401);
			});

			it('should respond with a status code 400 if the body have a no valid letter', async () => {
				const payload = {
					dna: ['ATGCGJ', 'CAGTGC', 'TTATCT', 'AAACGG', 'GCGTCA', 'TCACTG'],
				};

				const response = await supertest(app.app)
					.post('/api/mutation/')
					.set({ 'auth-token': `${token}`, Accept: 'application/json' })
					.send(payload);
				const errors = JSON.parse(response.text)?.errors[0].msg;

				expect(response.statusCode).toBe(400);
				expect(errors).toBe('Invalid letter J');
			});
			it('should respond with a status code 400 if the dna is register', async () => {
				const payload = {
					dna: ['ATGCGA', 'CAGTGC', 'TTATCT', 'AAACGG', 'GCGTCA', 'TCACTG'],
				};

				const response = await supertest(app.app)
					.post('/api/mutation/')
					.set({ 'auth-token': `${token}`, Accept: 'application/json' })
					.send(payload);
				const errors = JSON.parse(response.text)?.errors[0].msg;

				expect(response.statusCode).toBe(400);
				expect(errors).toBe(
					'this mutation is already registered in the database'
				);
			});
		});

		describe('success response test mutation path', () => {
			it('should respond with a status code 200 if the dna is success register', async () => {
				const payload = {
					dna: ['ATGCGA', 'CAGTGC', 'TTATCT', 'AAACGG', 'GCGTCA', 'TCACTA'],
				};
				const mutationId = payload.dna.join('-');

				const response = await supertest(app.app)
					.post('/api/mutation/')
					.set({ 'auth-token': `${token}`, Accept: 'application/json' })
					.send(payload)
					.expect(200);

				await mutationModel.findOneAndDelete({ mutationId });
			});
			it('should a return true boolean if array of dna have a mutation', async () => {
				const payload = {
					dna: ['ATGCGA', 'AAAAGC', 'TTATCT', 'CCCCGG', 'GCGTCA', 'TCACTA'],
				};
				const mutationId = payload.dna.join('-');

				const response = await supertest(app.app)
					.post('/api/mutation/')
					.set({ 'auth-token': `${token}`, Accept: 'application/json' })
					.send(payload);

				expect(response.body.mutation).toBe(true);

				await mutationModel.findOneAndDelete({ mutationId });
			});
			it('should a return false boolean if array of dna dont have a mutation', async () => {
				const payload = {
					dna: ['ATGCGA', 'CAGTGC', 'TTATAT', 'AGACGG', 'GCGTCA', 'TCACTG'],
				};
				const mutationId = payload.dna.join('-');

				const response = await supertest(app.app)
					.post('/api/mutation/')
					.set({ 'auth-token': `${token}`, Accept: 'application/json' })
					.send(payload);

				expect(response.body.mutation).toBe(false);

				await mutationModel.findOneAndDelete({ mutationId });
			});
		});
	});
	describe('GET - /api/mutation/stats', () => {
		describe('failed response test mutation path', () => {
			it('should respond with a status code 401 if the request does not have a jwt', async () => {
				await supertest(app.app).get('/api/mutation/stats').expect(401);
			});
		});
		describe('success response test mutation path', () => {
			it('should respond with a status code 200 if consult the mutations', async () => {
				await supertest(app.app)
					.get('/api/mutation/stats')
					.set({ 'auth-token': `${token}`, Accept: 'application/json' })
					.expect(200);
			});
		});
	});
});
