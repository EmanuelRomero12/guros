import supertest from 'supertest';
import mongosee from 'mongoose';
import Server from '../../infrastructure/config/server.config';
import { userModel } from '../../infrastructure/models/user.model';
const app = new Server();

describe('testing the user path', () => {
	afterAll(async () => {
		await mongosee.disconnect();
		await mongosee.connection.close();
	});
	describe('POST - /api/user', () => {
		describe('failed response test user path', () => {
			test('should respond with a status code 400 no body send', async () => {
				await supertest(app.app).post('/api/user').expect(400);
			});
			test('should respond with a 400 status code and a message indicating the credentials are duplicate', async () => {
				const body = {
					name: 'User Test',
					email: 'user.testing@test.com',
					password: 'test123',
				};

				const message = `The email ${body.email} is already registered in the database`;
				const response = await supertest(app.app).post('/api/user').send(body);
				const errors = JSON.parse(response.text)?.errors[0].msg;

				expect(errors).toBe(message);
			});
			test('should respond with a 400 status code and a message indicating the password is mandatory', async () => {
				const body = {
					name: 'User Test',
					email: 'user.testing23@test.com',
					password: 'test',
				};

				const message = `The password is mandatory and must contain more than 6 characters`;
				const response = await supertest(app.app).post('/api/user').send(body);
				const errors = JSON.parse(response.text)?.errors[0].msg;

				expect(errors).toBe(message);
				expect(response.statusCode).toBe(400);
			});
			test('should respond with a 400 status code and a message indicating this email format is not valid', async () => {
				const body = {
					name: 'User Test',
					email: 'user.testing23test.com',
					password: 'test123',
				};

				const message = `This email format is not valid`;
				const response = await supertest(app.app).post('/api/user').send(body);
				const errors = JSON.parse(response.text)?.errors[0].msg;

				expect(errors).toBe(message);
				expect(response.statusCode).toBe(400);
			});
			test('should respond with a 400 status code and a message indicating name is required', async () => {
				const body = {
					name: '',
					email: 'user.testing23test.com',
					password: 'test123',
				};

				const message = `Name is required`;
				const response = await supertest(app.app).post('/api/user').send(body);
				const errors = JSON.parse(response.text)?.errors[0].msg;

				expect(errors).toBe(message);
				expect(response.statusCode).toBe(400);
			});
		});
		describe('success response test user path', () => {
			test('should respond with a status code 201 and a message indicating user created successfully', async () => {
				const body = {
					name: 'User Test',
					email: 'user.testin234@test.com',
					password: 'test123',
				};

				const response = await supertest(app.app).post('/api/user').send(body);
				const message = JSON.parse(response.text).message;
				const userEmail = JSON.parse(response.text).user.email;

				expect(response.statusCode).toBe(201);
				expect(message).toBe('user created successfully');
				expect(userEmail).toBe(body.email);

				const { email } = body;
				await userModel.findOneAndDelete({ email });
			});
		});
	});
});
