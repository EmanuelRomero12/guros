import { MutationCount } from '../../infrastructure/interfaces/mutation.interface';
import { mutationModel } from '../../infrastructure/models/mutation.model';

const InsertMutation = async (mutationId: string) => {
	try {
		const findMutations = await findMutation(mutationId);

		if (!findMutations) {
			const dna = new mutationModel({ mutationId });
			if (dna) {
				await dna.save();
				return dna;
			}
		}
	} catch (error) {
		throw new Error('error inserting the dna id');
	}
};

const findMutation = async (mutationId: string) => {
	try {
		const mutation = await mutationModel.findOne({ mutationId });

		if (mutation) {
			return mutation;
		}
	} catch (error) {
		throw new Error('error inserting dna');
	}
};

const updateMutation = async (mutationId: object) => {
	try {
		await mutationModel.findOneAndUpdate(mutationId, { status: true });
	} catch (error) {
		throw new Error('error updating dna');
	}
};

const countMutations = async (): Promise<MutationCount[]> => {
	try {
		return await mutationModel.aggregate([
			{
				$group: {
					_id: 1,
					statusFalse: { $sum: { $cond: [{ $eq: ['$status', false] }, 1, 0] } },
					statusTrue: { $sum: { $cond: [{ $eq: ['$status', true] }, 1, 0] } },
				},
			},
		]);
	} catch (error) {
		throw new Error('error count the  dna');
	}
};
export { InsertMutation, updateMutation, countMutations, findMutation };
