import { Request } from 'express';
import {
	BodyMutation,
	MutationCount,
	MutationResponse,
} from '../../infrastructure/interfaces/mutation.interface';
import {
	countMutations,
	InsertMutation,
	updateMutation,
} from '../repositories/mutation.repository';

const hasMutation = async (request: Request): Promise<MutationResponse> => {
	const { dna } = request.body as BodyMutation;
	const dnaCode = await saveDna(dna);

	if (dnaCode) {
		const mutation = verifyMutation(dna);
		if (mutation) {
			await updateMutation({ mutationId: dnaCode });
			return { mutation };
		} else {
			return { mutation };
		}
	} else {
		throw new Error('something went wrong when saving the mutation');
	}
};

const saveDna = async (dna: string[]): Promise<string> => {
	const ids = dna.join('-');
	await InsertMutation(ids);
	return ids;
};

const verifyMutation = (dna: string[]): boolean => {
	let mutation = false;
	for (let i = 0; i < dna.length; i++) {
		const rowToArray = dna[i].split('');
		const rowLimit = rowToArray.length + 1;
		for (let l = 0; l < rowToArray.length; l++) {
			if (rowLimit - (l + 1) > 3) {
				if (
					rowToArray[l] === rowToArray[l + 1] &&
					rowToArray[l] === rowToArray[l + 2] &&
					rowToArray[l] === rowToArray[l + 3]
				) {
					return (mutation = true);
				}
			}

			if (dna.length + 1 - (i + 1) > 3) {
				if (
					dna[i].split('')[l] === dna[1].split('')[l] &&
					dna[i].split('')[l] === dna[2].split('')[l] &&
					dna[i].split('')[l] === dna[3].split('')[l]
				) {
					return (mutation = true);
				}

				if (
					dna[i].split('')[l] === dna[i + 1].split('')[l - 1] &&
					dna[i].split('')[l] === dna[i + 2].split('')[l - 2] &&
					dna[i].split('')[l] === dna[i + 3].split('')[l - 3]
				) {
					return (mutation = true);
				}
			}
		}

		for (let a = rowToArray.length - 1; a >= 0; a--) {
			if (dna.length + 1 - (i + 1) > 3) {
				if (
					dna[i].split('')[a] === dna[i + 1].split('')[a + 1] &&
					dna[i].split('')[a] === dna[i + 2].split('')[a + 2] &&
					dna[i].split('')[a] === dna[i + 3].split('')[a + 3]
				) {
					return (mutation = true);
				}
			}
		}
	}

	return mutation;
};

const mutationStastLogic = async (): Promise<MutationCount[]> => {
	return await countMutations();
};

export { hasMutation, mutationStastLogic };
