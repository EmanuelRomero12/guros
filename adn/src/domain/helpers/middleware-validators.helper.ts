import { Request, Response, NextFunction } from 'express';
import { validationResult } from 'express-validator';
import { BodyMutation } from '../../infrastructure/interfaces/mutation.interface';
import { findMutation } from '../repositories/mutation.repository';

const validateFields = (
	request: Request,
	response: Response,
	next: NextFunction
): void => {
	const errors = validationResult(request);
	if (!errors.isEmpty()) {
		response.status(400).json(errors);
		return;
	}

	next();
};

const validateWordsBody = (body: BodyMutation) => {
	const { dna } = body;
	const validWords = ['A', 'T', 'C', 'G'];

	if (Array.isArray(dna)) {
		if (!dna.length)
			throw new Error('the dna argument must have at least one dna sequence');

		for (const words of dna) {
			if (words.length < 6 || words.length > 6) {
				throw new Error(
					'DNA codes cannot contain less than 6 characters or more than 6'
				);
			}

			for (let i = 0; i < words.length; i++) {
				if (!validWords.includes(words[i])) {
					throw new Error(`Invalid letter ${words[i]}`);
				}
			}
		}

		return true;
	} else {
		throw new Error('the request body must be of type array: dna []');
	}
};

const validateIdMutations = async (body: BodyMutation) => {
	const mutationId = body.dna.join('-');

	const existMutation = await findMutation(mutationId);

	if (existMutation) {
		throw new Error('this mutation is already registered in the database');
	}
};

export { validateFields, validateWordsBody, validateIdMutations };
