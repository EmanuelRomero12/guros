import { User } from '../../infrastructure/interfaces/user.interface';
import { findUser } from '../repositories/users.repository';

const emailAlreadyExists = async (email: string): Promise<void> => {
	const { email: mail }: User = await findUser({ email });

	if (mail) {
		throw new Error(`The email ${mail} is already registered in the database`);
	}
};

export { emailAlreadyExists };
