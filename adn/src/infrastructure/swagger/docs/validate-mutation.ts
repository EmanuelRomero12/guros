export const validateMutation = {
	post: {
		tags: ['Mutation'],
		description: 'Validate Mutation',
		operationId: 'validateMutation',
		parameters: [],
		requestBody: {
			content: {
				'application/json': {
					schema: {
						$ref: '#/components/schemas/Mutation',
					},
				},
			},
		},

		responses: {
			200: {
				description: 'Boolean mutation result',
			},
			400: {
				description: 'Bad Request',
			},
			401: {
				description: 'User not allowed',
			},
			500: {
				description: 'Server error',
			},
		},
	},
};
