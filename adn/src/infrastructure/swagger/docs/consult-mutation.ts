export const consultMutation = {
	get: {
		tags: ['Mutation'],
		description: 'Consult Mutation',
		operationId: 'consultMutation',
		parameters: [],

		responses: {
			200: {
				description: 'success',
			},
			401: {
				description: 'User not allowed',
			},
			500: {
				description: 'Server error',
			},
		},
	},
};
