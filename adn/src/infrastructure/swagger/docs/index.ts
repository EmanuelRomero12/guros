import { consultMutation } from './consult-mutation';
import { createUser } from './create-user';
import { loginUser } from './login-user';
import { validateMutation } from './validate-mutation';

export const apis = {
	paths: {
		'/api/auth/login': {
			...loginUser,
		},
		'/api/user': {
			...createUser,
		},
		'/api/mutation': {
			...validateMutation,
		},
		'/api/mutation/stats': {
			...consultMutation,
		},
	},
};
