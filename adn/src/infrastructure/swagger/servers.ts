export const servers = {
	servers: [
		{
			url: 'https://guros-challenge-adn.herokuapp.com',
			description: 'Production server',
		},
		{
			url: 'http://localhost:3000',
			description: 'Local server',
		},
	],
};
