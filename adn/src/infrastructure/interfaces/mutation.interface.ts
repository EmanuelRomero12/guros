export interface MutationSchema {
	mutationId: string;
	ratio: number;
	status: boolean;
}

export interface BodyMutation {
	dna: string[];
}

export interface MutationResponse {
	mutation: boolean;
}

export interface MutationCount {
	statusFalse: number;
	statusTrue: number;
}
