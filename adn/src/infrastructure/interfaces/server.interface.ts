/*increment when a new route is added */
export interface Paths {
	auth: string;
	users: string;
	mutation: string;
	docs: string;
}
