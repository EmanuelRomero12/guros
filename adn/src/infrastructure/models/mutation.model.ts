import { Schema, model } from 'mongoose';
import { MutationSchema } from '../interfaces/mutation.interface';

const mutationSchema = new Schema<MutationSchema>({
	mutationId: {
		type: String,
		required: [true, 'mutationId is required'],
		unique: true,
	},
	status: {
		type: Boolean,
		default: false,
	},
});

export const mutationModel = model<MutationSchema>('Mutation', mutationSchema);
