import { Request, Response } from 'express';
import {
	hasMutation,
	mutationStastLogic,
} from '../../domain/services/mutation.service';
import { MutationCount } from '../interfaces/mutation.interface';

const mutationAdn = async (
	request: Request,
	response: Response
): Promise<void> => {
	try {
		const { mutation } = await hasMutation(request);

		response.status(200).json({ mutation });
	} catch (error) {
		console.log(error);
		response
			.status(500)
			.json({ message: 'An error occurred while registering the mutation' });
	}
};

const mutationAdnStats = async (
	request: Request,
	response: Response
): Promise<void> => {
	try {
		const [mutations]: MutationCount[] = await mutationStastLogic();
		const { statusFalse, statusTrue } = mutations;

		response.status(200).json({
			count_mutations: statusTrue,
			count_no_mutation: statusFalse,
			ratio: 0.4,
		});
	} catch (error) {
		response
			.status(500)
			.json({ message: 'An error occurred while count the mutation' });
	}
};

export { mutationAdn, mutationAdnStats };
