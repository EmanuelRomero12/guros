import * as express from 'express';
import {
	mutationAdn,
	mutationAdnStats,
} from '../controllers/mutation.controller';
import { verifyJwt } from '../middlewares/validate-jwt.middleware';
import {
	validateIdMutation,
	validateWords,
} from '../middlewares/validate-words-mutation.middleware';
export const mutation: express.Router = express.Router();

mutation.post('/', verifyJwt, validateWords, validateIdMutation, mutationAdn);

mutation.get('/stats', verifyJwt, mutationAdnStats);
