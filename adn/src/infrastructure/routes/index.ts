import { auth } from './auth.route';
import { mutation } from './mutation.route';
import { users } from './users.route';

export default {
	auth,
	users,
	mutation,
};
