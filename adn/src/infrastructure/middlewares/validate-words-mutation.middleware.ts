import { body } from 'express-validator';
import {
	validateFields,
	validateIdMutations,
	validateWordsBody,
} from '../../domain/helpers/middleware-validators.helper';

const validateWords = [
	body().custom((_noUsed, { req }) => validateWordsBody(req.body)),
	validateFields,
];

const validateIdMutation = [
	body().custom((_noUsed, { req }) => validateIdMutations(req.body)),
	validateFields,
];

export { validateWords, validateIdMutation };
