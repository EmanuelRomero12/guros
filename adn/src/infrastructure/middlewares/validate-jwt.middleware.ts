import { Request, Response, NextFunction } from 'express';
import jsonWebToken from 'jsonwebtoken';
import { findUserById } from '../../domain/repositories/users.repository';
import {
	invalidToken,
	unauthorizedUser,
} from '../../domain/constants/auth.constants';
import { JwtPayload } from '../interfaces/auth.interface';
import { User } from '../interfaces/user.interface';

export const verifyJwt = async (
	request: Request,
	response: Response,
	next: NextFunction
): Promise<void> => {
	const token: string = request.header('auth-token') || '';
	const secret: string = process.env.SECRET || '';

	if (!token || token === '') {
		response.status(401).json({
			message: unauthorizedUser,
		});
		return;
	}

	try {
		const { id: _id } = jsonWebToken.verify(token, secret) as JwtPayload;
		const user: User = await findUserById({ _id });

		if (!user.status) {
			response.status(401).json({
				message: unauthorizedUser,
			});
			return;
		}

		next();
	} catch (error) {
		console.log(error);
		response.status(401).json({
			message: invalidToken,
		});
	}
};
