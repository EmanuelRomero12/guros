# Guros ADN 📦
Challenge de codigo para la empresa guros 🚀.

## Instalación 👨‍💻

Usar el manejador de paquetes [npm](https://docs.npmjs.com/) para instalar los node modules correspondientes.

```bash
npm install
```

## Configurar variables de ambiente ⚙️

Leer el archivo .env.example posteriormente crear un archivo .env en la raiz del proyecto en su microservicio correspondiente.

```bash
#Port
PORT=3000

#enviroment
DEV=development

#Mongo credentials
MONGO_USER=
MONGO_PASSWORD=
MONGO_CNN=

#Json Web Token credentials
SECRET=
```

## Levantar el proyecto 🏋️‍♂️

Para levantar el proyecto usar el comando:

```bash
npm run start:dev
```

## Testing

Para iniciar las pruebas usar el comando:

```bash
npm run test
```

puedes visitar las rutas de los endpoints disponibles en la carpeta de src/infrastructure/config/server.config.ts

O puedes usar mi coleccion de postman se encuentra en la raiz del proyecto ;)

## Heroku deploy 🚀
He deployado el proyecto a un servidor en heroku puedes realizar pruebas desde ahí te dejo el link [aqui](https://guros-challenge-adn.herokuapp.com/api/docs). Debes autenticarte para poder realizar peticiones.
